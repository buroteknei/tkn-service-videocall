# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-22
### Released
- Basic configuration
## [1.0.1] - 2018-05-24
- Adds rest call for obtaining the picture related to the person's selfie
## [1.0.2] - 2018-05-30
- Adds rest operation for get the customer picture from quobis storage (no saving in TAS)
## [1.0.3] - 2018-07-02
- Se arreglan errores de BBDD relacionadas con el trigger de auditoría, por no especificar el valor para la columna userOpeCrea.
- Se corrige nombre de clave "QUOBIS-CLIENT", url = "${tkn.quobis.url}" por ${tkn.quobis.storage.url} para indicar la url de almacenamiento del selfie y la grabación en Quobis.
